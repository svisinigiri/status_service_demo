#!/bin/bash
 
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source_dir="$SCRIPT_DIR"
local_image=dealersocket/status
 
echo "Running maven build..." \
&& mvn -f "$source_dir"/pom.xml clean install \
&& echo "Building $local_image..." \
&& docker build -t $local_image "$source_dir" \
&& echo "Successfully built $local_image"