-- Create the status database
CREATE DATABASE statusdb;
 
-- Create the db user with default password
CREATE USER statusdb_user IDENTIFIED BY 'password';
 
-- Make db user admin of status database
GRANT ALL PRIVILEGES ON statusdb.* TO 'statusdb_user';
 
-- Switch to status database to create the schema
USE statusdb;
 
-- Begin Base Status Schema
CREATE TABLE inv_base_status (
base_status_id INT(11) not null,
base_status_name varchar(20) not null,
PRIMARY KEY (base_status_id)
);

-- Insert script for inv_base_status table
INSERT INTO inv_base_status (base_status_id, base_status_name)
VALUES (1, 'active'),
(2, 'wholesale'),
(3, 'sold'),
(4, 'remove'),
(5, 'upraised');

-- Begin Status Schema
CREATE TABLE inv_custom_status (
custom_status_id varchar(50) not null,
base_status_id INT(11) not null,
dealer_id varchar(20) not null,
custom_status_name varchar(20) not null,
description varchar(100),
synd_retail boolean not null default 0, 
synd_whsle boolean not null default 0,
iim_need boolean not null default 0,
iim_pref boolean not null default 0,
can_sell boolean not null default 0,
aging boolean not null default 0,
PRIMARY KEY (custom_status_id)
);
