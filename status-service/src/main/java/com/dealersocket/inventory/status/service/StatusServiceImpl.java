package com.dealersocket.inventory.status.service;

import com.dealersocket.inventory.status.model.Status;
import com.dealersocket.inventory.status.repository.StatusDao;
import com.dealersocket.inventory.status.repository.StatusRepository;
import javax.ws.rs.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * StatusService implementation class.
 * 
 * @author svisinigiri
 *
 */
@Service
public class StatusServiceImpl implements StatusService {
  private final StatusRepository repository;

  @Autowired
  public StatusServiceImpl(StatusRepository repository) {
    this.repository = repository;
  }


  /**
   * Create new custom status.
   * 
   * @param status the status object to save.
   * @return StatusDao
   */
  public StatusDao createStatus(Status status) {
    StatusDao statusDao = status.asStatusDao();
    return repository.save(statusDao);
  }

  /**
   * Update custom status.
   * 
   * @param status the status object to save.
   * @return StatusDao
   */
  public StatusDao updateStatus(Status status) {
    StatusDao statusDao = status.asStatusDao();
    return repository.save(statusDao);
  }

  /**
   * Delete custom status.
   * 
   * @param id the id to delete.
   */
  public void deleteStatus(String id) {
    StatusDao statusDao = repository.findOne(id);
    if (statusDao == null) {
      throw new NotFoundException("No status found for id " + id);
    }
    repository.delete(id);
  }

}
