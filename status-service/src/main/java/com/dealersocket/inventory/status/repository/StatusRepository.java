package com.dealersocket.inventory.status.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Interface for generic CRUD operations on a repository for a status.
 * @author svisinigiri
 *
 */
public interface StatusRepository extends PagingAndSortingRepository<StatusDao, String> {

}
