package com.dealersocket.inventory.status.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Status Dao object.
 * 
 * @author svisinigiri
 *
 */
@Entity(name = "inv_custom_status")
public class StatusDao {

  @Id
  @Column(name = "custom_status_id", nullable = false, length = 50)
  private String customStatusId;

  @Column(name = "base_status_id", nullable = false, length = 20)
  private Long baseStatusId;

  @Column(name = "dealer_id", nullable = false, length = 20)
  private String dealerId;

  @Column(name = "custom_status_name", nullable = false, length = 20)
  private String customStatusName;

  @Column(name = "description", nullable = false, length = 100)
  private String description;

  @Column(name = "synd_retail", nullable = false)
  private boolean syndRetail;

  @Column(name = "synd_whsle", nullable = false)
  private boolean syndWhsle;

  @Column(name = "iim_need", nullable = false)
  private boolean iimNeed;

  @Column(name = "iim_pref", nullable = false)
  private boolean iimPref;

  @Column(name = "can_sell", nullable = false)
  private boolean canSell;

  @Column(name = "aging", nullable = false, length = 10)

  private Long aging;

  /**
   * Get customStatusId.
   * 
   * @return the customStatusId
   */
  public String getCustomStatusId() {
    return customStatusId;
  }

  /**
   * Set customStatusId.
   * 
   * @param customStatusId the customStatusId to set
   */
  public void setCustomStatusId(String customStatusId) {
    this.customStatusId = customStatusId;
  }

  /**
   * Get baseStatusId.
   * 
   * @return the baseStatusId
   */
  public Long getBaseStatusId() {
    return baseStatusId;
  }

  /**
   * Set baseStatusId.
   * 
   * @param baseStatusId the baseStatusId to set
   */
  public void setBaseStatusId(Long baseStatusId) {
    this.baseStatusId = baseStatusId;
  }

  /**
   * Get dealerId.
   * 
   * @return the dealerId
   */
  public String getDealerId() {
    return dealerId;
  }

  /**
   * Set dealerId.
   * 
   * @param dealerId the dealerId to set
   */
  public void setDealerId(String dealerId) {
    this.dealerId = dealerId;
  }

  /**
   * Get customStatusName.
   * 
   * @return the customStatusName
   */
  public String getCustomStatusName() {
    return customStatusName;
  }

  /**
   * Set customStatusName.
   * 
   * @param customStatusName the customStatusName to set
   */
  public void setCustomStatusName(String customStatusName) {
    this.customStatusName = customStatusName;
  }

  /**
   * Get status description.
   * 
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Set status description.
   * 
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Get syndRetail.
   * 
   * @return the syndRetail
   */
  public boolean isSyndRetail() {
    return syndRetail;
  }

  /**
   * Set syndRetail.
   * 
   * @param syndRetail the syndRetail to set
   */
  public void setSyndRetail(boolean syndRetail) {
    this.syndRetail = syndRetail;
  }

  /**
   * Get syndWhsle.
   * 
   * @return the syndWhsle
   */
  public boolean isSyndWhsle() {
    return syndWhsle;
  }

  /**
   * Set syndWhsle.
   * 
   * @param syndWhsle the syndWhsle to set
   */
  public void setSyndWhsle(boolean syndWhsle) {
    this.syndWhsle = syndWhsle;
  }

  /**
   * Get iimNeed.
   * 
   * @return the iimNeed
   */
  public boolean isIimNeed() {
    return iimNeed;
  }

  /**
   * Set iimNeed.
   * 
   * @param iimNeed the iimNeed to set
   */
  public void setIimNeed(boolean iimNeed) {
    this.iimNeed = iimNeed;
  }

  /**
   * Get iimPref.
   * 
   * @return the iimPref
   */
  public boolean isIimPref() {
    return iimPref;
  }

  /**
   * Set iimPref.
   * 
   * @param iimPref the iimPref to set
   */
  public void setIimPref(boolean iimPref) {
    this.iimPref = iimPref;
  }

  /**
   * Get canSell.
   * 
   * @return the canSell
   */
  public boolean isCanSell() {
    return canSell;
  }

  /**
   * Set canSell.
   * 
   * @param canSell the canSell to set
   */
  public void setCanSell(boolean canSell) {
    this.canSell = canSell;
  }

  /**
   * Get status aging.
   * 
   * @return the aging
   */
  public Long getAging() {
    return aging;
  }

  /**
   * Set status aging.
   * 
   * @param aging the aging to set
   */
  public void setAging(Long aging) {
    this.aging = aging;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "StatusDao [customStatusId=" + customStatusId + ", baseStatusId=" + baseStatusId
        + ", dealerId=" + dealerId + ", customStatusName=" + customStatusName + ", description="
        + description + ", syndRetail=" + syndRetail + ", syndWhsle=" + syndWhsle + ", iimNeed="
        + iimNeed + ", iimPref=" + iimPref + ", canSell=" + canSell + ", aging=" + aging + "]";
  }


}
