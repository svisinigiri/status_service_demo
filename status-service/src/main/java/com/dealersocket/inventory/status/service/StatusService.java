package com.dealersocket.inventory.status.service;

import com.dealersocket.inventory.status.model.Status;
import com.dealersocket.inventory.status.repository.StatusDao;

/**
 * Interface for the status service.
 * 
 * @author svisinigiri
 *
 */
public interface StatusService {
  public StatusDao createStatus(Status status);

  public StatusDao updateStatus(Status status);

  public void deleteStatus(String id);
}
