package com.dealersocket.inventory.status;

import com.dealersocket.inv.spring.jersey.InventoryJerseyConfiguration;
import com.dealersocket.inv.spring.jersey.InventoryJerseyConfiguration.JerseyComponentsProvider;
import com.dealersocket.inventory.status.filter.AuthFilter;
import com.dealersocket.inventory.status.resource.StatusResource;
import java.util.Arrays;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Main class to bring up the spring boot application.
 *
 * @author svisinigiri
 *
 */
@Import(InventoryJerseyConfiguration.class)
@EnableJpaRepositories
@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class})
public class StatusApplication {
  static {
    System.setProperty("logging.level.com.dealersocket.inventory", "DEBUG");
    //System.setProperty("logging.level.com.dealersocket.inventory", "INFO");
  }

  public static void main(String[] args) {
    SpringApplication.run(StatusApplication.class, args);
  }

  /**
   * return JerseyComponentsProvider.
   *
   */
  @Bean
  public JerseyComponentsProvider jerseyComponentsProvider() {
    return () -> {
      return Arrays.asList(StatusResource.class, AuthFilter.class);
    };
  }

}
