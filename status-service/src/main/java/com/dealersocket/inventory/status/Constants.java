package com.dealersocket.inventory.status;

/**
 * File contains all constants.
 * 
 * @author svisinigiri
 *
 */
public class Constants {
  public static final String BASE_PATH = "status";
  public static final int HTTP_STATUS_CODE_400 = 400;
  public static final int HTTP_STATUS_CODE_401 = 401;
  public static final int HTTP_STATUS_CODE_404 = 404;
  public static final int HTTP_STATUS_CODE_500 = 500;
  public static final String HTTP_STATUS_MSG_400 = "Bad Request";
  public static final String HTTP_STATUS_MSG_401 = "Unauthorized";
  public static final String HTTP_STATUS_MSG_404 = "Not Found";
  public static final String HTTP_STATUS_MSG_500 = "Server Error";

}
