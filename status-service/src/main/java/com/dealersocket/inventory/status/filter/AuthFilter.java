package com.dealersocket.inventory.status.filter;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

@Provider
public class AuthFilter implements ContainerRequestFilter {

  private ResourceInfo resourceInfo;

  @Context
  public void setResourceInfo(ResourceInfo resourceInfo) {
    this.resourceInfo = resourceInfo;
  }

  @Override
  public void filter(ContainerRequestContext request) throws IOException {

    // collect all applied annotations
    List<Annotation> annotations = new ArrayList<>();
    annotations.addAll(Arrays.asList(resourceInfo.getResourceClass().getAnnotations()));
    annotations.addAll(Arrays.asList(resourceInfo.getResourceMethod().getAnnotations()));

    // determine if authentication is needed
    boolean unauthenticatedAllowed = true;
    for (Annotation annotation : annotations) {
      if (Authenticate.class.equals(annotation.annotationType())) {
        unauthenticatedAllowed = false;
        break;
      }
    }
    if (unauthenticatedAllowed) {
      return;
    }

    // extract header
    String authHeader = request.getHeaders().getFirst("Authorization");

    // make sure valid header
    if (authHeader == null || !authHeader.toLowerCase().startsWith("basic ")) {
      throw new WebApplicationException(Status.UNAUTHORIZED);
    }

    // make sure correct credentials
    if (!"admin:password".equals(authHeader.substring(6))) {
      throw new WebApplicationException(Status.UNAUTHORIZED);
    }
  }

}
