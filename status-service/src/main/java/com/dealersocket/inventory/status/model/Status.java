package com.dealersocket.inventory.status.model;

import com.dealersocket.inv.spring.jersey.content.response.hateoas.Resource;
import com.dealersocket.inventory.status.repository.StatusDao;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

/**
 * Status domain class.
 *
 * @author svisinigiri
 *
 */
@ApiModel(value = "Status", description = "Status resource representation")
public class Status extends Resource {
  private final String customStatusId;
  private final Long baseStatusId;
  private final String dealerId;
  private final String customStatusName;
  private final String description;
  private final boolean syndRetail;
  private final boolean syndWhsle;
  private final boolean iimNeed;
  private final boolean iimPref;
  private final boolean canSell;
  private final Long aging;

  /**
   * Create constructor for Status object.
   *
   */
  @JsonCreator
  public Status(@JsonProperty("customStatusId") String customStatusId,
      @JsonProperty("baseStatusId") Long baseStatusId, @JsonProperty("dealerId") String dealerId,
      @JsonProperty("customStatusName") String customerStatusName,
      @JsonProperty("description") String description,
      @JsonProperty("syndRetail") boolean syndRetail, @JsonProperty("syndWhsle") boolean syndWhsle,
      @JsonProperty("iimNeed") boolean iimNeed, @JsonProperty("iimPref") boolean iimPref,
      @JsonProperty("canSell") boolean canSell, @JsonProperty("aging") Long aging) {
    super();
    this.customStatusId = customStatusId;
    this.baseStatusId = baseStatusId;
    this.dealerId = dealerId;
    this.customStatusName = customerStatusName;
    this.description = description;
    this.syndRetail = syndRetail;
    this.syndWhsle = syndWhsle;
    this.iimNeed = iimNeed;
    this.iimPref = iimPref;
    this.canSell = canSell;
    this.aging = aging;
  }


  /**
   * Create status DAO object from status domain object.
   *
   * @return StatusDAO
   */
  public StatusDao asStatusDao() {
    StatusDao statusDao = new StatusDao();
    statusDao.setCustomStatusId(this.customStatusId);
    statusDao.setBaseStatusId(this.baseStatusId);
    statusDao.setDealerId(this.dealerId);
    statusDao.setCustomStatusName(this.customStatusName);
    statusDao.setDescription(this.description);
    statusDao.setSyndRetail(this.syndRetail);
    statusDao.setSyndWhsle(this.syndWhsle);
    statusDao.setIimNeed(this.iimNeed);
    statusDao.setIimPref(this.iimPref);
    statusDao.setCanSell(this.canSell);
    statusDao.setAging(this.aging);
    return statusDao;
  }


  /**
   * Get status aging.
   * 
   * @return the aging
   */
  public Long getAging() {
    return this.aging;
  }


  /**
   * Get base status id.
   * 
   * @return the baseStatusId
   */
  public Long getBaseStatusId() {
    return this.baseStatusId;
  }


  /**
   * Get custom status id.
   * 
   * @return the customStatusId
   */
  public String getCustomStatusId() {
    return this.customStatusId;
  }


  /**
   * Get custom status name.
   * 
   * @return the customStatusName
   */
  public String getCustomStatusName() {
    return this.customStatusName;
  }


  /**
   * Get dealer id.
   * 
   * @return the dealerId.
   */
  public String getDealerId() {
    return this.dealerId;
  }


  /**
   * Get status description.
   * 
   * @return the description
   */
  public String getDescription() {
    return this.description;
  }


  /**
   * Set canSell.
   * 
   * @return the canSell.
   */
  public boolean isCanSell() {
    return this.canSell;
  }


  /**
   * Set iimNeed.
   * 
   * @return the iimNeed
   */
  public boolean isIimNeed() {
    return this.iimNeed;
  }


  /**
   * Get iimPref.
   * 
   * @return the iimPref.
   */
  public boolean isIimPref() {
    return this.iimPref;
  }


  /**
   * Get syndRetail.
   * 
   * @return the syndRetail.
   */
  public boolean isSyndRetail() {
    return this.syndRetail;
  }


  /**
   * Get syndWhsle.
   * 
   * @return the syndWhsle.
   */
  public boolean isSyndWhsle() {
    return this.syndWhsle;
  }


  /**
   * Set aging.
   * 
   * @param aging the aging to set.
   */
  public void setAging(Long aging) {
    // this.aging = aging;
  }


  /**
   * Set baseStatusId.
   * 
   * @param baseStatusId the baseStatusId to set.
   */
  public void setBaseStatusId(Long baseStatusId) {
    // this.baseStatusId = baseStatusId;
  }


  /**
   * Set canSell.
   * 
   * @param canSell the canSell to set.
   */
  public void setCanSell(boolean canSell) {
    // this.canSell = canSell;
  }


  /**
   * Set customStatusId.
   * 
   * @param customStatusId the customStatusId to set.
   */
  public void setCustomStatusId(String customStatusId) {
    // this.customStatusId = customStatusId;
  }


  /**
   * Set customStatusName.
   * 
   * @param customStatusName the customStatusName to set.
   */
  public void setCustomStatusName(String customStatusName) {
    // this.customStatusName = customStatusName;
  }


  /**
   * Set dealerId.
   * 
   * @param dealerId the dealerId to set.
   */
  public void setDealerId(String dealerId) {
    // this.dealerId = dealerId;
  }


  /**
   * Set status description.
   * 
   * @param description the description to set.
   */
  public void setDescription(String description) {
    // this.description = description;
  }


  /**
   * Set iimNeed.
   * 
   * @param iimNeed the iimNeed to set.
   */
  public void setIimNeed(boolean iimNeed) {
    // this.iimNeed = iimNeed;
  }


  /**
   * Set iimPref.
   * 
   * @param iimPref the iimPref to set.
   */
  public void setIimPref(boolean iimPref) {
    // this.iimPref = iimPref;
  }


  /**
   * Set syndRetail.
   * 
   * @param syndRetail the syndRetail to set
   */
  public void setSyndRetail(boolean syndRetail) {
    // this.syndRetail = syndRetail;
  }

  /**
   * Set syndWhsle.
   * 
   * @param syndWhsle the syndWhsle to set
   */
  public void setSyndWhsle(boolean syndWhsle) {
    // this.syndWhsle = syndWhsle;
  }

  /**
   * Override toString method.
   *
   * @return String
   */
  @Override
  public String toString() {
    return "Status [customStatusId=" + this.customStatusId + ", baseStatusId=" + this.baseStatusId
        + ", dealerId=" + this.dealerId + ", customStatusName=" + this.customStatusName
        + ", description=" + this.description + ", syndRetail=" + this.syndRetail + ", syndWhsle="
        + this.syndWhsle + ", iimNeed=" + this.iimNeed + ", iimPref=" + this.iimPref + ", canSell="
        + this.canSell + ", aging=" + this.aging + "]";
  }
}
