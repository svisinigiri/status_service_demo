package com.dealersocket.inventory.status.resource;

import com.dealersocket.inv.spring.jersey.content.response.error.ErrorMessage;
import com.dealersocket.inv.spring.jersey.content.response.hateoas.Hateoas;
import com.dealersocket.inventory.status.Constants;
import com.dealersocket.inventory.status.filter.Authenticate;
import com.dealersocket.inventory.status.model.Status;
import com.dealersocket.inventory.status.repository.StatusDao;
import com.dealersocket.inventory.status.service.StatusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Controller class for the status.
 * 
 * @author svisinigiri
 *
 */
@Api("Status")
@Path(Constants.BASE_PATH)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Authenticate
@Component
public class StatusResource {
  private static final Logger log = LoggerFactory.getLogger(StatusResource.class);
  private final Hateoas hateoas;
  private UriInfo uriInfo;

  private final StatusService service;

  @Autowired
  public StatusResource(StatusService service, Hateoas hateoas) {
    this.service = service;
    this.hateoas = hateoas;
  }

  @Context
  public void setUriInfo(UriInfo uriInfo) {
    this.uriInfo = uriInfo;
  }

  /**
   * Create new custom status.
   * 
   * @param status the status object to create.
   * @return Status
   */
  @POST
  @ApiOperation(value = "Create Status")
  @ApiResponses(value = {
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_400, message = Constants.HTTP_STATUS_MSG_400,
          response = ErrorMessage.class),
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_401, message = Constants.HTTP_STATUS_MSG_401,
          response = ErrorMessage.class),
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_500, message = Constants.HTTP_STATUS_MSG_500,
          response = ErrorMessage.class)})
  @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Basic user:pass",
      required = true, dataType = "string", paramType = "header"))
  public Status createStatus(@Valid Status status) {
    log.info("status creation in createStatus method");
    StatusDao statusDao = service.createStatus(status);
    status = constructStatus(statusDao);
    status.addLink("self",
        hateoas.buildFullUri(uriInfo.getPath() + "/" + status.getCustomStatusId()));
    log.debug("Status " + status.getCustomStatusName() + " created " + " for the dealer "
        + status.getDealerId());
    return status;
  }

  /**
   * Update custom status.
   * 
   * @param status the status object to update
   * @return Status
   */
  @PUT
  @ApiOperation(value = "Update Status")
  @ApiResponses(value = {
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_400, message = Constants.HTTP_STATUS_MSG_400,
          response = ErrorMessage.class),
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_401, message = Constants.HTTP_STATUS_MSG_401,
          response = ErrorMessage.class),
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_500, message = Constants.HTTP_STATUS_MSG_500,
          response = ErrorMessage.class)})
  @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Basic user:pass",
      required = true, dataType = "string", paramType = "header"))
  public Status updateStatus(@Valid Status status) {
    log.info("status updation in updateStatus method");
    StatusDao statusDao = service.updateStatus(status);
    status = constructStatus(statusDao);
    status.addLink("self",
        hateoas.buildFullUri(uriInfo.getPath() + "/" + status.getCustomStatusId()));
    log.debug("Status " + status.getCustomStatusName() + " updated " + " for the dealer "
        + status.getDealerId());
    return status;
  }

  @Path("/{id}")
  @DELETE
  @ApiOperation(value = "Delete Status")
  @ApiResponses(value = {
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_401, message = Constants.HTTP_STATUS_MSG_401,
          response = ErrorMessage.class),
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_404, message = Constants.HTTP_STATUS_MSG_404,
          response = ErrorMessage.class),
      @ApiResponse(code = Constants.HTTP_STATUS_CODE_500, message = Constants.HTTP_STATUS_MSG_500,
          response = ErrorMessage.class)})
  @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Basic user:pass",
      required = true, dataType = "string", paramType = "header"))
  public void deleteStatus(@PathParam("id") String id) {
    service.deleteStatus(id);
    log.debug("Status deleted for the id " + id);
  }

  /**
   * Construct status object from statusDao object.
   * 
   * @param StatusDao the statusDao object to construct.
   * @return Status
   */
  private Status constructStatus(StatusDao statusDao) {
    return new Status(statusDao.getCustomStatusId(), statusDao.getBaseStatusId(),
        statusDao.getDealerId(), statusDao.getCustomStatusName(), statusDao.getDescription(),
        statusDao.isSyndRetail(), statusDao.isSyndWhsle(), statusDao.isIimNeed(),
        statusDao.isIimPref(), statusDao.isCanSell(), statusDao.getAging());
  }

}
