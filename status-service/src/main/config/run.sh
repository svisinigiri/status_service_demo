#!/bin/bash
 
# This exposes the following Docker environment variables
 
# Logging level: log_level (TRACE, DEBUG, INFO, WARN, ERROR)
 
# Application Version: version (Sets what's displayed for version at /info)
 
# MySQL
# mysql_host (Default: statusdb)
# mysql_port (Default: 3306)
# mysql_db (Default: statusdb)
# mysql_user (Default: statusdb_user)
# mysql_password (Default: password)
 
# HATEOAS: base_url (Default: [blank string])
 
# InfluxDB Metrics
# influx_enabled (Default: false)
# influx_db (Default: statusdb)
# influx_host (Default: influxdb)
# influx_port (Default: 8086)
 
# Swagger: swagger_enabled (Default: false)
 
exec java \
    -Dlogging.level.com.dealersocket="${log_level:-INFO}" \
    -Dinfo.app.name="Status Service" \
    -Dinfo.app.description="Status micorservice that demonstrates how to use spring-boot-jersey-starter" \
    -Dinfo.app.version="${version:-1.0.0}" \
    -Dspring.datasource.url="jdbc:mysql://${mysql_host:-statusdb}:${mysql_port:-3306}/${mysql_db:-statusdb}" \
    -Dspring.datasource.username=${mysql_user:-statusdb_user} \
    -Dspring.datasource.password=${mysql_password:-password} \
    -Dspring.datasource.driverClassName=${mysql_jdbc_driver:-com.mysql.cj.jdbc.Driver} \
    -Dinv.hateoas.baseUrl="${base_url}" \
    -Dinv.metrics.influxdb.enabled=${influx_enabled:-false} \
    -Dinv.metrics.influxdb.database=${influx_db:-statusdb} \
    -Dinv.metrics.influxdb.host=${influx_host:-influxdb} \
    -Dinv.metrics.influxdb.port=${influx_port:-8086} \
    -Dinv.swagger.enabled=${swagger_enabled:-false} \
    -Dinv.swagger.appId=status-service \
    -Dinv.swagger.title="Status Service" \
    -Dinv.swagger.basePackage=com.dealersocket.inventory.status.resource \
    -jar /data/status-service.jar