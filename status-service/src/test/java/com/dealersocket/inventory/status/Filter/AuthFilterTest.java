package com.dealersocket.inventory.status.Filter;

import java.util.Arrays;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import com.dealersocket.inventory.status.filter.AuthFilter;
import com.dealersocket.inventory.status.filter.Authenticate;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.stereotype.Component;
 
@SuppressWarnings({ "rawtypes", "unchecked" })
public class AuthFilterTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  private final AuthFilter authFilter = new AuthFilter();
  private final ResourceInfo mockedResourceInfo = Mockito.mock(ResourceInfo.class);

  public AuthFilterTest() {

      Mockito.when(mockedResourceInfo.getResourceClass()).thenReturn((Class) NeedsAuthentication.class);
      Mockito.when(mockedResourceInfo.getResourceMethod()).thenReturn(NeedsAuthentication.class.getMethods()[0]);

      authFilter.setResourceInfo(mockedResourceInfo);
  }

  @Test
  public void test_filter_throws_exception_if_asked_to_authenticate_and_auth_header_missing() throws Exception {

      // verification of exception setup
      expectedException.expect(WebApplicationException.class);
      expectedException.expectMessage("HTTP 401 Unauthorized");

      // setup
      MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
      ContainerRequestContext request = Mockito.mock(ContainerRequestContext.class);
      Mockito.when(request.getHeaders()).thenReturn(headers);

      // run
      authFilter.filter(request);
  }

  @Test
  public void test_filter_throws_exception_if_asked_to_authenticate_and_auth_header_malformed() throws Exception {

      // verification of exception setup
      expectedException.expect(WebApplicationException.class);
      expectedException.expectMessage("HTTP 401 Unauthorized");

      // setup
      MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
      headers.put("Authorization", Arrays.asList("bad format"));
      ContainerRequestContext request = Mockito.mock(ContainerRequestContext.class);
      Mockito.when(request.getHeaders()).thenReturn(headers);

      // run
      authFilter.filter(request);
  }

  @Test
  public void test_filter_throws_exception_if_asked_to_authenticate_and_credentials_invalid() throws Exception {

      // verification of exception setup
      expectedException.expect(WebApplicationException.class);
      expectedException.expectMessage("HTTP 401 Unauthorized");

      // setup
      MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
      headers.put("Authorization", Arrays.asList("basic wrong:credentials"));
      ContainerRequestContext request = Mockito.mock(ContainerRequestContext.class);
      Mockito.when(request.getHeaders()).thenReturn(headers);

      // run
      authFilter.filter(request);
  }

  @Test
  public void test_filter_does_not_throw_exception_if_asked_to_authenticate_and_credentials_valid() throws Exception {

      // setup
      MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
      headers.put("Authorization", Arrays.asList("basic admin:password"));
      ContainerRequestContext request = Mockito.mock(ContainerRequestContext.class);
      Mockito.when(request.getHeaders()).thenReturn(headers);

      // run
      authFilter.filter(request);
  }

  @Test
  public void test_filter_does_not_throw_exception_if_not_asked_to_authenticate() throws Exception {

      // setup
      Mockito.when(mockedResourceInfo.getResourceClass()).thenReturn((Class) DoesNotNeedAuthentication.class);

      MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
      headers.put("Authorization", Arrays.asList("basic wrong:credentials"));
      ContainerRequestContext request = Mockito.mock(ContainerRequestContext.class);
      Mockito.when(request.getHeaders()).thenReturn(headers);

      // run
      authFilter.filter(request);
  }

  @Test
  public void test_filter_does_not_care_about_case_of_basic() throws Exception {

      // setup
      MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
      headers.put("Authorization", Arrays.asList("Basic admin:password"));
      ContainerRequestContext request = Mockito.mock(ContainerRequestContext.class);
      Mockito.when(request.getHeaders()).thenReturn(headers);

      // run
      authFilter.filter(request);
  }

  @Authenticate
  public static class NeedsAuthentication {

      public void someMethod() {

      }

  }

  @Component
  public static class DoesNotNeedAuthentication {

      public void anotherMethod() {

      }
  }

}
